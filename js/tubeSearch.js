/**
 * Created by Takeshi Fukuhara
 * @Kobe beef
 */
var APIKEY = "AIzaSyAaUUQ3IcWLE-tO0LGm9l8fW_xYdxQZR2U";

var PlaylistIDs;
var PlayListNames;
var PlaylistMenuHtml = "";
var VideoIDs;

function checkIfTokenIsActive() {
    var accessToken = $.cookie('access_token');
    var timeout = $.cookie('expires_in');
    var timeStamp = $.cookie('token_timeStamp');

    if (accessToken == undefined || timeout == undefined || timeStamp == undefined)
        return false;

    // check if token is not expired
    var currentTime = (new Date()).getTime();
    if ((currentTime - timeStamp) / 1000 >= timeout)
        return false;

    return true;
}

function listPlaylistsIfAvailable() {
    if (checkIfTokenIsActive()) {
        var accessToken = $.cookie('access_token');
        listWatchLater(accessToken);
        listupOtherPlaylists(accessToken);
    }
    else{
        setupLoginButton();
    }
}

function listWatchLater(accessToken) {
    var requestOptions = {
        part: "contentDetails,snippet",
        mine: "true",
        access_token: accessToken
    };
    var request = gapi.client.request({
        path: "youtube/v3/channels",
        params: requestOptions
    });
    request.execute(function (resp) {
        setTimeout(function() {
            if (resp.error) {
                setupLoginButton();
            } else {
                showAccountIcon(resp);
                addWatchLaterName(resp);
            }
        }, 50);
    });
}

function showAccountIcon(resp) {
    var html = '<img id="account_icon" src="' + resp.items[0].snippet.thumbnails.default.url + '">';
    html += '<div id="playlist_dropdown" class="wrapper-dropdown-2"></div>';
    html += '<div id="account-control" class="arrow_box">';
    html += '<a href="https://www.youtube.com/playlist?list=WL" target="_blank">Edit Watch Later at Youtube</a>';
    html += '<br>';
    html += '<a href="https://www.youtube.com/channel/' + resp.items[0].id + '/playlists" target="_blank">Edit PLAYLYSTs at Youtube</a>';
    html += '<div id="sign-out-button" class="sign-out-area">';
    html += '<span class="sign-out-text">Sign out</span>'
    html += '</div>';
    html += '</div>'
    $('.login_area').html = '';
    $(html).appendTo('.login_area');

    $("#account_icon").click(function(event){
        $("#account-control").toggleClass('arrow_box_active');
        event.stopPropagation();
    });

    $("#sign-out-button").click(function(){
        disconnectUser();
    });

    $(document).on('click', function(event) {
        if (!$(event.target).closest('#account-control').length) {
            $("#account-control").removeClass('arrow_box_active');
        }
    });
}

function disconnectUser() {
    var access_token = $.cookie('access_token');
    var revokeUrl = 'https://accounts.google.com/o/oauth2/revoke?token=' +
        access_token;

    // Perform an asynchronous GET request.
    $.ajax({
        type: 'GET',
        url: revokeUrl,
        async: false,
        contentType: "application/json",
        dataType: 'jsonp',
        success: function(nullResponse) {
            $.removeCookie('access_token');
            location.reload();
        },
        error: function(e) {
            // Handle the error
            // console.log(e);
            // You could point users to manually disconnect if unsuccessful
            // https://plus.google.com/apps
        }
    });
}

function addWatchLaterName(resp) {
    $("#menu").html(""); //clear

    var watchLaterId= resp.items[0].contentDetails.relatedPlaylists.watchLater;
    var watchLaterText = "Watch Later";

    PlaylistMenuHtml = "";
    PlaylistMenuHtml += '<span class="playlist_title">PLAYLISTs</span>';
    PlaylistMenuHtml += '<ul class="dropdown">';
    PlaylistMenuHtml += '<li><a href="javascript:void(0)" onclick="showPlaylistItems(\'' + watchLaterId + '\',\'' + watchLaterText + '\')">';
    PlaylistMenuHtml += 'Watch Later</a></li>';
}

function listupOtherPlaylists(accessToken) {
    var requestOptions = {
        part: "snippet",
        mine: "true",
        access_token: accessToken
    };
    var request = gapi.client.request({
        path: "/youtube/v3/playlists",
        params: requestOptions
    });
    request.execute(function (resp) {
        setTimeout(function() {
            if (resp.error) {
                $("#message").html(resp.error.message);
            } else {
                addPlaylistsNames(resp);
            }
        }, 100);
    });
}

function addPlaylistsNames(resp){
    PlaylistIDs = new Array(resp.items.length);
    PlayListNames = new Array(resp.items.length);

    for (var i = 0; i < resp.items.length; i++) {
        PlaylistIDs[i] = resp.items[i].id;
        PlayListNames[i] = resp.items[i].snippet.title;

        PlaylistMenuHtml += '<li><a href="javascript:void(0)" onclick="showPlaylistItems(PlaylistIDs[\'' + i + '\'], PlayListNames[\'' + i + '\'])">';
        PlaylistMenuHtml += resp.items[i].snippet.title + '</a></li>';
    }

    PlaylistMenuHtml += '</ul>';

    setTimeout(function(){
        $(PlaylistMenuHtml).appendTo("#playlist_dropdown");
        $('#playlist_dropdown').show();
        setupDropDown();
    }, 100);
}

function showPlaylistItems(playlistId, playlistName){
    $("#result_text").text(playlistName);
    $('.search_results').html("");

    var accessToken = $.cookie('access_token');

    var requestOptions = {
        part: "snippet",
        playlistId: playlistId,
        maxResults: 20,
        access_token:accessToken
    };
    var request = gapi.client.request({
        path:"/youtube/v3/playlistItems",
        params:requestOptions
    });
    request.execute(function(resp) {
        if (resp.error) {
            $("#message").html(resp.error.message);
        } else {
            queryVideoDetails(resp);
        }
    });
}

function showRelatedVideos(videoID){
    $("#result_text").text("Related Videos");

    var requestOptions = {
        maxResults: 20,
        part: "id",
        type: "video",
        videoEmbeddable: "true",
        relatedToVideoId: videoID,
        order: "relevance"
    };
    var request = gapi.client.request({
        mine:"",
        path:"/youtube/v3/search",
        params:requestOptions
    });
    request.execute(function(resp) {
        if (resp.error) {
            $("#message").html(resp.error.message);
        } else {
            queryVideoDetails(resp);
        }
    });
}

function showSearchResults() {
    $("#result_text").text("Search Results (" + $('#keyword_input').val() + ")");

    var requestOptions = {
        q: $('#keyword_input').val(),
        maxResults: 20,
        part: "id",
        type: "video",
        videoEmbeddable: "true",
        order: "relevance"
    };
    var request = gapi.client.request({
        mine:"",
        path:"/youtube/v3/search",
        params:requestOptions
    });
    request.execute(function(resp) {
        if (resp.error) {
            $("#message").html(resp.error.message);
        } else {
            queryVideoDetails(resp);
        }
    });
}

function queryVideoDetails(resp) {
    var ids = "";
    for (var i = 0; i < resp.items.length; i++){
        var videoId;
        if (resp.items[i].id.videoId != undefined){
            videoId = resp.items[i].id.videoId;
        }
        else{
            videoId = resp.items[i].snippet.resourceId.videoId;
        }

        ids += videoId;
        ids += ",";
    }

    var requestOptions = {
        maxResults: 20,
        part: "contentDetails,statistics,snippet",
        id: ids
    };
    var request = gapi.client.request({
        mine:"",
        path:"/youtube/v3/videos",
        params:requestOptions
    });
    request.execute(function(resp) {
        if (resp.error) {
            $("#message").html(resp.error.message);
        } else {
            createResults(resp);
        }
    });
}

function createResults(resp){
    VideoIDs = new Array(resp.items.length);
    SearResultHtmls = new Array(resp.items.length);

    $(".search_results").html(""); //clear

    var html = '';
    html += '<ul class="result_list">';
    for (var i = 0; i < resp.items.length; i++) {
        if (resp.items[i].id.videoId != undefined) {
            VideoIDs[i] = resp.items[i].id.videoId;
        }
        else{
            VideoIDs[i] = resp.items[i].id;
        }

        html+= '<li class="list_item">';
        html += '<a href="javascript:void(0)" onclick="loadVideo(VideoIDs[\'' + i + '\'])">';
        html += '<span class="image_span">';
        html += '<img src=' + resp.items[i].snippet.thumbnails.default.url  + '>';
        html += '<span class="video_time">' + convertVideoDuration(resp.items[i].contentDetails.duration) + '</span>';
        html += '</span>';
        html += '<span class="video_title">' + resp.items[i].snippet.title + '</span>';
        html += '<span class="video_owner">' + 'by ' + resp.items[i].snippet.channelTitle + '</span>';
        html += '<span class="view_count">' + resp.items[i].statistics.viewCount + ' views' + '</span>';
        html += '</a>';
        html += '</li>';
    }
    html += '</ul>';
    $(html).hide().appendTo('.search_results').fadeIn('medium');
}

function convertVideoDuration(duration) {
    var a = duration.match(/\d+/g);

    if (duration.indexOf('M') >= 0 && duration.indexOf('H') == -1 && duration.indexOf('S') == -1) {
        a = [0, a[0], 0];
    }

    if (duration.indexOf('H') >= 0 && duration.indexOf('M') == -1) {
        a = [a[0], 0, a[1]];
    }
    if (duration.indexOf('H') >= 0 && duration.indexOf('M') == -1 && duration.indexOf('S') == -1) {
        a = [a[0], 0, 0];
    }

    if (a.length == 3) {
        return a[0] + ':' + formatTime(a[1]) + ':' + formatTime(a[2]);
    }

    if (a.length == 2) {
        return a[0] + ':' + formatTime(a[1]);
    }

    if (a.length == 1) {
        return '0:' + a[0];
    }
    return duration
}

function formatTime(time){
    if (time < 10){
        return '0' + time;
    }

    return time;
}

function setupLoginButton() {
    $('.login_area').html = '';
    var html = '<div id="login_button_div" class="login-button">';
    html += '<span class="login-text">Sign in to Youtube</span>';
    html += '</div>';
    $(html).appendTo('.login_area');

    $("#login_button_div").click(function(){
        var uri = URI + "/oauthRedirect.html";
        var encoded = encodeURIComponent(uri);
        var authUrl = "https://accounts.google.com/o/oauth2/auth?client_id=469880112957-nhrd3dubn487of350lgse4p09bappljh.apps.googleusercontent.com&redirect_uri="
            + encoded + "&scope=https://www.googleapis.com/auth/youtube&response_type=token";
        window.location.replace(authUrl);
    });
}

function onJSClientLoad() {
    setupYoutubePlayer();

    gapi.client.setApiKey(APIKEY);
    gapi.client.load('youtube', 'v3');

    window.setTimeout(function(){
        listPlaylistsIfAvailable();
    }, 1);
}

$(function() {
    $('#search').click(showSearchResults);
    $( '#keyword_input' ).keypress( function ( e ) {
        if ( e.which == 13 ) {
            $('#keyword_input').autocomplete("close");
            showSearchResults();
            return false;
        }
    } );
});

