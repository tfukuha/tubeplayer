/**
 * Created by Takeshi Fukuhara on 2014/08/04.
 * @Kobe Beef
 */
var URI = "http://localhost:63342/TubePlayer";
var IndexURI = "/index.html";
//var URI= "http://tubeplayer.web.fc2.com";
//var IndexURI = "";


var player;
var startTime;
var stopTime;
var TimerReference;
var PlaybackRates;
var IsInRepeatMode = false;

var aborted = false;
var timer;

var LoopSetKey = {
    None: 0,
    Start: 5,
    Stop: 10
};
var CurrentLoopSetKey = LoopSetKey.None;
var CurrentSliderStartStop;

function onYouTubeIframeAPIReady() {

    var videoID = getParameterByName("v");
    if (videoID == "") {
        videoID = 'PKcaoQxzBeM';
    }

    var playsInlineValue = 1;
    var ua = findOsAndIE();
    if (ua.isiPhone || ua.isiPod) playsInlineValue = 0;

    player = new YT.Player('player', {
        height: '390',
        width: '640',
        videoId: videoID,
        playerVars: {
//                controls: 0,
            showinfo: 0,
            modestbranding: 1,
            wmode: "transparent",
            html5: 1,
            iv_load_policy: 3,
            playsinline: playsInlineValue
        },
        events: {
            'onReady': onPlayerReady,
            'onStateChange': onPlayerStateChange
        }
    });
}

// 4. The API will call this function when the video player is ready.
function onPlayerReady(event) {
    PlaybackRates = event.target.getAvailablePlaybackRates();
    setTimeout(function () {
        var html = '';
        var count = 1;
        if (PlaybackRates.length > 1) {
            PlaybackRates.forEach(function (val) {
                html += '<div>';
                html += '<input id="Radio' + count + '" type="radio" class="radio" name="radio"';
                if (val == 1) html += ' checked="check" ';
                html += ' onclick="player.setPlaybackRate(' + val + ')"/>';
                html += '<label for="Radio' + count + '">x' + val + '</label>';
                html += '</div>';

                count++;
            });

            $(html).appendTo('#playback_speed');
        }

        //Show Video Title
        var title = event.target.getVideoData().title;
        $("#video_name").text(title);

        //Show Related Videos
        var videoID = event.target.getVideoData().video_id;
        showRelatedVideos(videoID);

        $("#play-pause-button").click(playVideo);
        setupSlider();
    }, 1);
}

function formatVideoDuration(value) {
    console.log("formattedResult(source value) = " + value);
    if (CurrentLoopSetKey == LoopSetKey.Start &&
        CurrentSliderStartStop[0] == value) value = startTime;
    if (CurrentLoopSetKey == LoopSetKey.Stop &&
        CurrentSliderStartStop[1] == value) value = stopTime;

    value = Math.round(value);

    var hours = Math.floor(value / 3600);
    var restOfTimes = value - hours * 3600;
    var mins = Math.floor(restOfTimes / 60);
    var seconds = restOfTimes - mins * 60;
    var zeroText = "0";
    if (hours < 1) zeroText = '';

    var result = (hours < 1 ? '' : hours + ':')
        + (mins < 10 ? zeroText + mins : mins) + ":"
        + ( seconds < 10 ? "0" + seconds : seconds );
    console.log("formattedResult = " + result);

    return result;
}

function setupSlider() {
    var videoDuration = player.getDuration();
    var html = '<input id="Slider" type="slider" name="time" value="0;' + videoDuration + '"/>';
    $(html).appendTo('#slider-div');

    $("#Slider").slider({ from: 0,
        to: videoDuration,
        scale: [0, '|', formatVideoDuration(videoDuration * 0.25), '|', formatVideoDuration(videoDuration * 0.5), '|',
            formatVideoDuration(videoDuration * 0.75), '|', formatVideoDuration(videoDuration)],
        limits: false,
        step: 1,
        calculate: function (value) {
            return formatVideoDuration(value);
        },
        onstatechange: function (value) {
            CurrentSliderStartStop = value.split(';');
            console.log("onStateChange:startTime = " + CurrentSliderStartStop[0] + ",stopTime = " + CurrentSliderStartStop[1]);

            if (CurrentLoopSetKey == LoopSetKey.None) {
                startTime = CurrentSliderStartStop[0];
                stopTime = CurrentSliderStartStop[1];
                if (IsInRepeatMode) startFromQueueTime();
            }
        },
        skin: "round_plastic" });
}

function onPlayerStateChange(event) {
    const $playPauseButton = $("#play-pause-button");
    switch (event.data) {
        case YT.PlayerState.ENDED:/* 0 (再生終了（＝最後まで再生した）) */
            toggleToPlayButton();
            break;
        case YT.PlayerState.PLAYING:/* 1 (再生中) */
            aborted = false;
            clearInterval(timer);
            timer = setInterval("checkTime()", 10);

            $("#play-pause-button").show();
            toggleToPauseButton();
            break;
        case YT.PlayerState.PAUSED:/* 2 (一時停止された) */
            clearInterval(timer);
            toggleToPlayButton();
            break;
        case YT.PlayerState.BUFFERING:/* 3 (バッファリング中) */
            break;
        case YT.PlayerState.CUED:/* 5 (頭出しされた) */
            break;
        case -1:/* -1 (未スタート、他の動画に切り替えた時など) */
            clearInterval(timer);
            toggleToPlayButton();
            break;
    }
}

function toggleToPlayButton() {
    const $playPauseButton = $("#play-pause-button");
    $playPauseButton.off('click', playVideo);
    $playPauseButton.on('click', playVideo);
    $playPauseButton.addClass("playButton");
    $playPauseButton.removeClass("pauseButton");
    $("#pause-icon").hide();
    $("#play-icon").show();
}

function toggleToPauseButton() {
    const $playPauseButton = $("#play-pause-button");
    $playPauseButton.off('click', pauseVideo);
    $playPauseButton.on('click', pauseVideo);
    $playPauseButton.removeClass("playButton");
    $playPauseButton.addClass("pauseButton");
    $("#pause-icon").show();
    $("#play-icon").hide();
}

function checkTime() {
    if (IsInRepeatMode && (player.getCurrentTime() > stopTime)) {
        startFromQueueTime();
    }
}

function pauseVideo() {
    player.pauseVideo();
}

function playVideo() {
    if (IsInRepeatMode) {
        var currentTime = player.getCurrentTime();
        if (currentTime < startTime || stopTime < currentTime) startFromQueueTime();
    }

    player.playVideo();
}

function loadVideo(videoID) {
    window.location.href = URI + IndexURI + "?v=" + videoID;
}

function getStartTime() {
    startTime = Math.round(player.getCurrentTime());
    console.log("getStartTime = " + startTime);
    $("#Slider").slider("value", startTime, stopTime);
}

function getStopTime() {
    stopTime = Math.round(player.getCurrentTime());
    $("#Slider").slider("value", startTime, stopTime)
}

function startFromQueueTime() {
    player.seekTo(startTime, true);
}

function rewindAction(amount) {
    var currentTime = player.getCurrentTime();
    player.seekTo(currentTime - amount, true);
}

function forwardAction(amount) {
    var currentTime = player.getCurrentTime();
    player.seekTo(currentTime + amount, true);
}

function clearRewindTimer() {
    clearInterval(rewindTimer);
}

function clearTimer(timer) {
    event.preventDefault();                     // ページが動いたり、反応を止める（A タグなど）
    clearInterval(timer);
}

function setupFastRwdButton() {
    $("#fastRwdBtn").mousedown(function (e) {
        if (e.which == 1) {
            rewindAction(5);
            TimerReference = setInterval("rewindAction(5)", 500);
        }
    });
    $("#fastRwdBtn").mouseup(function () {
        clearInterval(TimerReference);
    });
    $("#fastRwdBtn").mouseout(function () {
        clearInterval(TimerReference);
    });

    $('#fastRwdBtn').bind('touchstart', function () {
        event.preventDefault();                     // ページが動いたり、反応を止める（A タグなど）
        rewindAction(5);
        TimerReference = setInterval("rewindAction(5)", 500);
    });
    $('#fastRwdBtn').bind('touchend', function () {
        clearTimer(TimerReference);
    });
    $('#fastRwdBtn').bind('touchleave', function () {
        clearTimer(TimerReference);
    });
    $('#fastRwdBtn').bind('touchcancel', function () {
        clearTimer(TimerReference);
    });
}

function setupRwdButton() {
    $("#rwdBtn").mousedown(function (e) {
        if (e.which == 1) {
            rewindAction(1);
            TimerReference = setInterval("rewindAction(1)", 300);
        }
    });
    $("#rwdBtn").mouseup(function () {
        clearInterval(TimerReference);
    });
    $("#rwdBtn").mouseout(function () {
        clearInterval(TimerReference);
    });

    $('#rwdBtn').bind('touchstart', function () {
        event.preventDefault();                     // ページが動いたり、反応を止める（A タグなど）
        rewindAction(1);
        TimerReference = setInterval("rewindAction(1)", 300);
    });
    $('#rwdBtn').bind('touchend', function () {
        clearTimer(TimerReference);
    });
    $('#rwdBtn').bind('touchleave', function () {
        clearTimer(TimerReference);
    });
    $('#rwdBtn').bind('touchcancel', function () {
        clearTimer(TimerReference);
    });
}
function setupFwdButton() {
    $("#fwdBtn").mousedown(function (e) {
        if (e.which == 1) {
            forwardAction(1);
            TimerReference = setInterval("forwardAction(1)", 400);
        }
    });
    $("#fwdBtn").mouseup(function () {
        clearInterval(TimerReference);
    });
    $("#fwdBtn").mouseout(function () {
        clearInterval(TimerReference);
    });

    $('#fwdBtn').bind('touchstart', function () {
        event.preventDefault();                     // ページが動いたり、反応を止める（A タグなど）
        forwardAction(1);
        TimerReference = setInterval("forwardAction(1)", 400);
    });
    $('#fwdBtn').bind('touchend', function () {
        clearTimer(TimerReference);
    });
    $('#fwdBtn').bind('touchleave', function () {
        clearTimer(TimerReference);
    });
    $('#fwdBtn').bind('touchcancel', function () {
        clearTimer(TimerReference);
    });
}

function setupFastFwdButton() {
    $("#fastFwdBtn").mousedown(function (e) {
        if (e.which == 1) {
            forwardAction(5);
            TimerReference = setInterval("forwardAction(5)", 500);
        }
    });
    $("#fastFwdBtn").mouseup(function () {
        clearInterval(TimerReference);
    });
    $("#fastFwdBtn").mouseout(function () {
        clearInterval(TimerReference);
    });

    $('#fastFwdBtn').bind('touchstart', function () {
        event.preventDefault();                     // ページが動いたり、反応を止める（A タグなど）
        forwardAction(5);
        TimerReference = setInterval("forwardAction(5)", 500);
    });

    $('#fastFwdBtn').bind('touchend', function () {
        clearTimer(TimerReference);
    });
    $('#fastFwdBtn').bind('touchleave', function () {
        clearTimer(TimerReference);
    });
    $('#fastFwdBtn').bind('touchcancel', function () {
        clearTimer(TimerReference);
    });
}

function rwdLoopStartPoint() {
    var temp = startTime - 1;
    startTime = Math.max(0, temp);
    console.log("getStartTime = " + startTime);

    CurrentLoopSetKey = LoopSetKey.Start;
    $("#Slider").slider("value", startTime, stopTime);
    CurrentLoopSetKey = LoopSetKey.None;

    if (IsInRepeatMode) startFromQueueTime();
}

function fwdLoopStartPoint() {
    var temp = startTime + 1;
    startTime = Math.min(stopTime, temp);
    console.log("getStartTime = " + startTime);

    CurrentLoopSetKey = LoopSetKey.Start;
    $("#Slider").slider("value", startTime, stopTime);
    CurrentLoopSetKey = LoopSetKey.None;

    if (IsInRepeatMode) startFromQueueTime();
}

function rwdLoopStopPoint() {
    var temp = stopTime - 1;
    stopTime = Math.max(startTime, temp);

    CurrentLoopSetKey = LoopSetKey.Stop;
    $("#Slider").slider("value", startTime, stopTime);
    CurrentLoopSetKey = LoopSetKey.None;
}

function fwdLoopStopPoint() {
    var temp = stopTime + 1;
    stopTime = Math.min(player.getDuration(), temp);

    CurrentLoopSetKey = LoopSetKey.Stop;
    $("#Slider").slider("value", startTime, stopTime);
    CurrentLoopSetKey = LoopSetKey.None;
}

function setupLoopSettingButtons() {
    $("#loopStartSet").on('mousedown touchstart', function (e) {
        if (e.which == 1) {
            CurrentLoopSetKey = LoopSetKey.Start;
            getStartTime();
            CurrentLoopSetKey = LoopSetKey.None;
        }
    });

    $("#loopStopSet").on('mousedown touchstart', function () {
        CurrentLoopSetKey = LoopSetKey.Stop;
        getStopTime();
        CurrentLoopSetKey = LoopSetKey.None;
    });

    // LoopStartRwd
    $("#loopStartRwd").on('mousedown', function (e) {
        if (e.which == 1) {
            rwdLoopStartPoint();
            TimerReference = setInterval(function () {
                rwdLoopStartPoint();
            }, 200);
        }
    });

    $('#loopStartRwd').on('touchstart', function () {
        event.preventDefault();                     // ページが動いたり、反応を止める（A タグなど）
        rwdLoopStartPoint();
        TimerReference = setInterval(function () {
            rwdLoopStartPoint();
        }, 200);
    });

    $("#loopStartRwd").on('mouseup mouseout touchend touchleave touchcancel', function () {
        clearInterval(TimerReference);
    });

    // LoopStartFwd
    $("#loopStartFwd").on('mousedown', function (e) {
        if (e.which == 1) {
            fwdLoopStartPoint();
            TimerReference = setInterval(function () {
                fwdLoopStartPoint();
            }, 200);
        }
    });

    $('#loopStartFwd').on('touchstart', function () {
        event.preventDefault();                     // ページが動いたり、反応を止める（A タグなど）
        fwdLoopStartPoint();
        TimerReference = setInterval(function () {
            fwdLoopStartPoint();
        }, 200);
    });

    $("#loopStartFwd").on('mouseup mouseout touchend touchleave touchcancel', function () {
        clearInterval(TimerReference);
    });

    //Loop Stop Rwd
    $("#loopStopRwd").on('mousedown', function (e) {
        if (e.which == 1) {
            rwdLoopStopPoint();
            TimerReference = setInterval(function () {
                rwdLoopStopPoint();
            }, 200);
        }
    });

    $('#loopStopRwd').on('touchstart', function () {
        event.preventDefault();                     // ページが動いたり、反応を止める（A タグなど）
        rwdLoopStopPoint();
        TimerReference = setInterval(function () {
            rwdLoopStopPoint();
        }, 200);
    });

    $("#loopStopRwd").on('mouseup mouseout touchend touchleave touchcancel', function () {
        clearInterval(TimerReference);
    });

    //Loop Stop Fwd
    $("#loopStopFwd").on('mousedown', function (e) {
        if (e.which == 1) {
            fwdLoopStopPoint();
            TimerReference = setInterval(function () {
                fwdLoopStopPoint();
            }, 200);
        }
    });

    $('#loopStopFwd').on('touchstart', function () {
        event.preventDefault();                     // ページが動いたり、反応を止める（A タグなど）
        fwdLoopStopPoint();
        TimerReference = setInterval(function () {
            fwdLoopStopPoint();
        }, 200);
    });

    $("#loopStopFwd").on('mouseup mouseout touchend touchleave touchcancel', function () {
        clearInterval(TimerReference);
    });

}

function setupYoutubePlayer() {
    var tag = document.createElement('script');

    tag.src = "https://www.youtube.com/iframe_api";
    var firstScriptTag = document.getElementsByTagName('script')[0];
    firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
}

function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results == null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}

function findOsAndIE() {
    var ua = {};
    ua.name = window.navigator.userAgent.toLowerCase();

    ua.isIE = (ua.name.indexOf('msie') >= 0 || ua.name.indexOf('trident') >= 0);
    ua.isiPhone = ua.name.indexOf('iphone') >= 0;
    ua.isiPod = ua.name.indexOf('ipod') >= 0;
    ua.isiPad = ua.name.indexOf('ipad') >= 0;
    ua.isiOS = (ua.isiPhone || ua.isiPod || ua.isiPad);
    ua.isAndroid = ua.name.indexOf('android') >= 0;
    ua.isTablet = (ua.isiPad || (ua.isAndroid && ua.name.indexOf('mobile') < 0));

    if (ua.isIE) {
        ua.verArray = /(msie|rv:?)\s?([0-9]{1,})([\.0-9]{1,})/.exec(ua.name);
        if (ua.verArray) {
            ua.ver = parseInt(ua.verArray[2], 10);
        }
    }
    if (ua.isiOS) {
        ua.verArray = /(os)\s([0-9]{1,})([\_0-9]{1,})/.exec(ua.name);
        if (ua.verArray) {
            ua.ver = parseInt(ua.verArray[2], 10);
        }
    }
    if (ua.isAndroid) {
        ua.verArray = /(android)\s([0-9]{1,})([\.0-9]{1,})/.exec(ua.name);
        if (ua.verArray) {
            ua.ver = parseInt(ua.verArray[2], 10);
        }
    }

    return ua;
}

$(function () {
    setupFastRwdButton();
    setupRwdButton();
    setupFwdButton();
    setupFastFwdButton();
    setupLoopSettingButtons();

    var ua = findOsAndIE();
    if (ua.isiOS || ua.isAndroid) {
        $("#play-pause-button").hide();
    }
    if (ua.isiPhone || ua.isiPod) {
        $("#fastRwdBtn").hide();
        $("#rwdBtn").hide();
        $("#fwdBtn").hide();
        $("#fastFwdBtn").hide();
    }

    $("#normalSpeedRadio").attr('checked', 'checked');
    $("#pause-icon").hide();
    $("#repeat-button").on('click', function () {
        var $repeatButton = $("#repeat-button");
        if (IsInRepeatMode) {
            $repeatButton.removeClass("repeatEnabled");
            $repeatButton.addClass("repeatDisabled");
            IsInRepeatMode = false;
        }
        else {
            $repeatButton.removeClass("repeatDisabled");
            $repeatButton.addClass("repeatEnabled");
            IsInRepeatMode = true;

            var currentTime = player.getCurrentTime();
            if (currentTime < startTime || stopTime < currentTime) startFromQueueTime();
        }
    });
});
