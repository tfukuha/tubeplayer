/**
 * Created by Fukuhara on 2014/08/30.
 */

function DropDown(el) {
    this.dd = el;
    this.initEvents();
}

DropDown.prototype = {
    initEvents : function() {
        var obj = this;

        obj.dd.on('click', function(event){
            $(this).toggleClass('active');
            event.stopPropagation();
        });
    }
}

function setupDropDown(){
    var dd = new DropDown( $('#playlist_dropdown') );

    $(document).click(function() {
        // all dropdowns
        $('.wrapper-dropdown-2').removeClass('active');
    });
}
//$(function() {
//
//    var dd = new DropDown( $('#playlist_dropdown') );
//
//    $(document).click(function() {
//        // all dropdowns
//        $('.wrapper-dropdown-2').removeClass('active');
//    });
//
//});
